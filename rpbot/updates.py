from typing import Text
import textwrap

from pydriller import Repository, Commit
from discord.ext import commands
from discord import TextChannel, Member, Guild as DiscordGuild
from discord.permissions import Permissions
from sqlalchemy.sql.expression import delete

from . import logger
from .sql import Guild, ChangelogChannel
from .db import Session

logger = logger.getChild("updates")

class Updates(commands.Cog):
    def __init__(self, bot:commands.Bot):
        self.bot = bot
    
    @commands.Cog.listener()
    async def on_ready(self):
        with Session() as s:
            channels = s.query(ChangelogChannel).all()
        
        for _channel in channels:
            channel:TextChannel = self.bot.get_channel(_channel.channel_id)
            commits = Repository(".", from_commit=_channel.last_revision).traverse_commits()
            if _channel.last_revision is not None:
                next(commits)
            await channel.send(textwrap.dedent("""\
            New RPBot Version Deployed!
            """))
            for commit in commits:
                await channel.send(textwrap.dedent(f"""\
                ```
                {commit.msg}
                ```
                """))
                _channel.last_revision = commit.hash
            with Session() as s:
                s.add(_channel)
                s.commit()

    @commands.command()
    async def updates_channel(self, ctx:commands.Context, channel:TextChannel=None):
        """
        Marks the current channel as the Changelog Updates channel
        """
        if channel is None:
            channel = ctx.channel
        _logger = logger.getChild("updates_channel")

        _logger.info("Got command")
        author:Member = ctx.author
        permissions:Permissions = author.permissions_in(ctx.channel)
        _logger.info("Checking if author has admin permissions: %s", permissions.administrator)
        if not permissions.administrator:
            _logger.info("Author does not have admin permissions")
            await ctx.channel.send(f"Only admins can use this command {author.mention}!", delete_after=10)
            await ctx.message.delete()
            return
        
        async with ctx.channel.typing():
            with Session() as session:
                if session.query(Guild).filter_by(guild_id=ctx.guild.id).count() != 1:
                    guild = Guild(guild_id=ctx.guild.id)
                    session.add(guild)
                if session.query(ChangelogChannel).filter_by(channel_id=channel.id).count() == 1:
                    await ctx.channel.send(f"Channel already marked as a Changelog Updates channel {ctx.author.mention}!", delete_after=10)
                    await ctx.message.delete()
                    return
                _channel = ChangelogChannel(channel_id=channel.id, guild_id=ctx.guild.id)
                session.add(_channel)
                session.commit()
            await ctx.channel.send(f"Added channel {channel.mention} to list of Changelog Updates channels.", delete_after=10)
            await ctx.message.delete()
            return


def setup(bot):
    bot.add_cog(Updates(bot))