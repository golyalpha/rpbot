from os import getenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base

engine = create_engine(getenv("DATABASE_URL", "sqlite:///test.db"))
Session = sessionmaker(engine)

Base = declarative_base()