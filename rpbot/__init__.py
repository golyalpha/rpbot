__version__ = '0.1.0'

import logging
import os

import discord
import discord.ext.commands

from . import db, sql

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("RPBot")

intents = discord.Intents(messages=True, reactions=True)



bot = discord.ext.commands.Bot("rp!", proxy=os.getenv("HTTPS_PROXY", os.getenv("https_proxy")))

bot.load_extension(".characters", package=__name__)
bot.load_extension(".updates", package=__name__)

@bot.listen()
async def on_ready():
    appinfo = await bot.application_info()
    await appinfo.owner.send("Bot Online")