from typing import Text
import textwrap
from discord.ext import commands
from discord import RawReactionActionEvent, TextChannel, Member, Guild as DiscordGuild, Message, Role as DiscordRole
from discord.permissions import Permissions
from sqlalchemy.sql.expression import delete

from . import logger
from .sql import Guild, Channel, Role
from .db import Session

logger = logger.getChild("characters")


class Characters(commands.Cog):
    def __init__(self, bot:commands.Bot):
        self.bot = bot
    
    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload:RawReactionActionEvent):
        _logger = logger.getChild("reaction_add_event")
        _logger.info("Got a reaction event")
        if payload.event_type != "REACTION_ADD":
            return
        
        _logger.info("Reaction event type check passed")
        message_id = payload.message_id
        channel_id = payload.channel_id
        guild_id = payload.guild_id

        # guild:DiscordGuild = self.bot.get_guild(guild_id)
        channel:TextChannel = self.bot.get_channel(channel_id)
        message:Message = await channel.fetch_message(message_id)
        member:Member = payload.member
        
        _logger.info("Retrieveing server config")
        with Session() as session:
            if session.query(Channel).filter_by(guild_id=guild_id, channel_id=channel_id).count() != 1:
                _logger.info("Channel not in whitelist")
                return
            channel_ref = session.query(Channel).filter_by(guild_id=guild_id, channel_id=channel_id).one()
            role_whitelist = [role.role_id for role in session.query(Role).filter_by(guild_id=guild_id).all()]
        _logger.info(role_whitelist)

        _logger.info("Whitelist passed, sending typing event")
        async with channel.typing():
            _logger.info("Check if author is me")
            if member.id == self.bot.user.id:
                return
            
            _logger.info("Check emoji type")
            _logger.info(payload.emoji.name)
            _logger.info(payload.emoji.name not in ["\N{WHITE HEAVY CHECK MARK}", "\N{NEGATIVE SQUARED CROSS MARK}"])
            if payload.emoji.name not in ["\N{WHITE HEAVY CHECK MARK}", "\N{NEGATIVE SQUARED CROSS MARK}"]:
                return
            
            _logger.info("Check if author has whitelisted role")
            for role in member.roles:
                if role.id in role_whitelist:
                    break
            else:
                _logger.info("Author doesn't have whitelisted role.")
                await channel.send(f"Only members with whitelisted roles can vote on characters {member.mention}!", delete_after=10)
                await message.remove_reaction(payload.emoji, member)
                return
            
            _logger.info("Counting votes")
            yea = 0
            nay = 0
            for reaction in message.reactions:
                if reaction.emoji == "\N{WHITE HEAVY CHECK MARK}":
                    yea+=reaction.count
                    continue
                if reaction.emoji == "\N{NEGATIVE SQUARED CROSS MARK}":
                    nay+=reaction.count
                    continue
            _logger.info("Got %d yea, %d nay", yea, nay)
            
            if yea > nay*2:
                target_channel:TextChannel = self.bot.get_channel(channel_ref.forward_channel_id)
                files = [await attachment.to_file() for attachment in message.attachments]
                await target_channel.send(message.content, files=files)
                await message.delete()
    
    @commands.Cog.listener()
    async def on_message(self, message:Message):
        _logger = logger.getChild("on_message")
        if message.author.id == self.bot.user.id:
            return
        
        with Session() as session:
            if session.query(Channel).filter_by(guild_id=message.guild.id, channel_id=message.channel.id).count() != 1:
                _logger.info("Channel not in whitelist")
                return
            roles = [message.guild.get_role(role.role_id) for role in session.query(Role).filter_by(guild_id=message.guild.id).all()]
        _logger.info(roles)

        if message.reference is not None and not message.is_system():
            old_message:Message = await message.channel.fetch_message(message.reference.message_id)
            if old_message.author != self.bot.user:
                await message.channel.send(f"Sorry {message.author.mention}, I can't modify a message that isn't a character profile.", delete_after=30)
                return
            if message.author not in old_message.mentions:
                await message.channel.send(f"Only the character owner can modify a character {message.author.mention}!", delete_after=15)
                return
            if len(message.attachments) > 0:
                new_files = [ await attachment.to_file() for attachment in message.attachments ]
                old_files = [ await attachment.to_file() for attachment in old_message.attachments ]
                old_files.extend(new_files)
                new_message = await message.channel.send(
                    old_message.content,
                    files=old_files
                )
                await new_message.add_reaction("\N{WHITE HEAVY CHECK MARK}")
                await new_message.add_reaction("\N{NEGATIVE SQUARED CROSS MARK}")
                await message.channel.send("Character sheet updated, votes reset. " + " ".join([role.mention for role in roles]), delete_after=60)
                await message.delete()
                await old_message.delete()
            return

            

        if len(message.content) < 500 and any([attachment.content_type == "text/plain" for attachment in message.attachments]):
            await message.channel.send(textwrap.dedent(f"""\
            Hey {message.author.mention}, your character sheet is under 500 characters. That doesn't seem right!
            If you posted a link to a Google Doc, please copy-paste the key information (your character's basic info, skills, items, etc.) here,
            some character reviwers may find it difficult to review a Google Doc character sheet, and it's useful to have the important
            information "frozen"."""), delete_after=60)
            return
        
        if len(message.mentions) > 0:
            await message.channel.send(textwrap.dedent(f"""\
            Sorry {message.author.mention}, a character profile cannot mention users - don't worry, your own tag will be added, and 
            people with the relevant roles will be notified of a new character automatically.
            """, delete_after=60))
            return

        new = await message.channel.send(
            message.content + f"\n\nCharacter by {message.author.mention}",
            files=[ await attachment.to_file() for attachment in message.attachments ]
        )
        await new.add_reaction("\N{WHITE HEAVY CHECK MARK}")
        await new.add_reaction("\N{NEGATIVE SQUARED CROSS MARK}")
        await message.channel.send(" ".join([role.mention for role in roles]), delete_after=60)
        await message.delete()
    
    @commands.command()
    async def review_channel(self, ctx:commands.Context, channel:TextChannel=None):
        """
        Marks the current channel as a Character Review channel
        """
        if channel is None:
            channel = ctx.channel
        _logger = logger.getChild("review_channel")

        _logger.info("Got command")
        author:Member = ctx.author
        permissions:Permissions = author.permissions_in(ctx.channel)
        _logger.info("Checking if author has admin permissions: %s", permissions.administrator)
        if not permissions.administrator:
            _logger.info("Author does not have admin permissions")
            await ctx.channel.send(f"Only admins can use this command {author.mention}!", delete_after=10)
            await ctx.message.delete()
            return
        
        async with ctx.channel.typing():
            with Session() as session:
                if session.query(Guild).filter_by(guild_id=ctx.guild.id).count() != 1:
                    guild = Guild(guild_id=ctx.guild.id)
                    session.add(guild)
                if session.query(Channel).filter_by(channel_id=channel.id).count() == 1:
                    await ctx.channel.send(f"Channel already marked as a Character Review channel {ctx.author.mention}!", delete_after=10)
                    await ctx.message.delete()
                    return
                _channel = Channel(channel_id=channel.id, guild_id=ctx.guild.id)
                session.add(_channel)
                session.commit()
            await ctx.channel.send(f"Added channel {channel.mention} to list of Character Review channels.", delete_after=10)
            await ctx.message.delete()
            return
    
    @commands.command()
    async def reviewed_channel(self, ctx:commands.Context, review_channel:TextChannel, target_channel:TextChannel=None):
        """
        Marks the current channel as the target channel for revied characters
        """
        if target_channel is None:
            target_channel = ctx.channel
        _logger = logger.getChild("reviewed_channel")

        _logger.info("Got command")
        author:Member = ctx.author
        permissions:Permissions = author.permissions_in(ctx.channel)
        _logger.info("Checking if author has admin permissions: %s", permissions.administrator)
        if not permissions.administrator:
            _logger.info("Author does not have admin permissions")
            await ctx.channel.send(f"Only admins can use this command {author.mention}!", delete_after=10)
            await ctx.message.delete()
            return
        
        async with ctx.channel.typing():
            with Session() as session:
                if session.query(Guild).filter_by(guild_id=ctx.guild.id).count() != 1:
                    guild = Guild(guild_id=ctx.guild.id)
                    session.add(guild)
                if session.query(Channel).filter_by(channel_id=review_channel.id).count() != 1:
                    await ctx.channel.send(f"Review channel doesn't exist!", delete_after=10)
                    await ctx.message.delete()
                    return
                _channel = session.query(Channel).filter_by(channel_id=review_channel.id).one()
                _channel.forward_channel_id = target_channel.id
                session.add(_channel)
                session.commit()
            await ctx.channel.send(f"Marked channel {target_channel.mention} as the target channel for reviewd characters in {review_channel.mention}.", delete_after=10)
            await ctx.message.delete()
            return
    
    @commands.command()
    async def reviewer_role(self, ctx:commands.Context, role:DiscordRole):
        """
        Allows the given role to review characters
        """
        _logger = logger.getChild("reviewer_role")

        _logger.info("Got command")
        author:Member = ctx.author
        permissions:Permissions = author.permissions_in(ctx.channel)
        _logger.info("Checking if author has admin permissions: %s", permissions.administrator)
        if not permissions.administrator:
            _logger.info("Author does not have admin permissions")
            await ctx.channel.send(f"Only admins can use this command {author.mention}!", delete_after=10)
            await ctx.message.delete()
            return
        
        async with ctx.channel.typing():
            with Session() as session:
                if session.query(Guild).filter_by(guild_id=ctx.guild.id).count() != 1:
                    guild = Guild(guild_id=ctx.guild.id)
                    session.add(guild)
                if session.query(Role).filter_by(role_id=role.id).count() == 1:
                    await ctx.channel.send(f"Role already allowed to review characters {ctx.author.mention}!", delete_after=10)
                    await ctx.message.delete()
                    return
                _role = Role(role_id=role.id, guild_id=ctx.guild.id)
                session.add(_role)
                session.commit()
            await ctx.channel.send(f"Allowed role {role.mention} to review characters.", delete_after=10)
            await ctx.message.delete()
            return



def setup(bot):
    bot.add_cog(Characters(bot))