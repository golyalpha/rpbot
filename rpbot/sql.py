from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import null
from sqlalchemy.sql.schema import ForeignKey, Column
from sqlalchemy.sql.sqltypes import Integer, String
from .db import Base


class Guild(Base):
    __tablename__ = 'guilds'

    id = Column(Integer, primary_key=True)
    guild_id = Column(Integer, unique=True, nullable=False)

    channels = relationship("Channel")
    roles = relationship("Role")


class Channel(Base):
    __tablename__ = "channels"

    id = Column(Integer, primary_key=True)
    channel_id = Column(Integer, unique=True, nullable=False)
    guild_id = Column(Integer, ForeignKey("guilds.id"))
    forward_channel_id = Column(Integer, nullable=True)


class ChangelogChannel(Base):
    __tablename__ = "changelog-channels"

    id = Column(Integer, primary_key=True)
    channel_id = Column(Integer, unique=True)
    guild_id = Column(Integer, ForeignKey("guilds.id"))
    last_revision = Column(String, nullable=True)

class Role(Base):
    __tablename__ = "roles"

    id = Column(Integer, primary_key=True)
    role_id = Column(Integer, unique=True, nullable=False)
    guild_id = Column(Integer, ForeignKey("guilds.id"))