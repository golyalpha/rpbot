from os import getenv

from rpbot import bot, db

db.Base.metadata.create_all(db.engine)

bot.run(getenv("DISCORD_TOKEN"))