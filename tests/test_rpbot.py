import toml
from rpbot import __version__


def test_version():
    ver = toml.load("pyproject.toml")["tool"]["poetry"]["version"]
    assert __version__ == ver
